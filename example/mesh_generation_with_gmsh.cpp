#include <gmsh.h>
#include <vector>

#include "geometry/Geometry_kernel.h"

#include "mesh/TriangleMesh.h"
#include "mesh/TetrahedronMesh.h"

#include "mesh/VTKMeshWriter.h"
typedef OF::Mesh::VTKMeshWriter Writer;

typedef OF::Geometry_kernel<double, int> GK;
typedef GK::Point_3 Node;
typedef GK::Vector_3 Vector;

typedef OF::Mesh::TriangleMesh<GK, Node, Vector> TriMesh;
typedef OF::Mesh::TetrahedronMesh<GK, Node, Vector> TetMesh;

void construct_mesh2d(TriMesh * mesh)
{
  std::vector<long unsigned int> nodeTag; //网格中每个节点的 tag
  std::vector<double> nodeCoord; //每个节点的坐标
  std::vector<double> nodeParaCoord; //每个节点的参数坐标, 暂时不知道什么用
  gmsh::model::mesh::getNodes(nodeTag, nodeCoord, nodeParaCoord);

  int dim = mesh->geo_dimension();

  int NN = nodeTag.size();
  auto & node = mesh->nodes();
  auto & cell = mesh->cells();
  node.resize(NN);
  for(int i = 0; i < NN; i++)
  {
    node[i][0] = nodeCoord[3*i+0];
    node[i][1] = nodeCoord[3*i+1];
    if(dim == 3)
      node[i][2] = nodeCoord[3*i+2];
  }

  std::map<int, int> nTag2Nid; //node 的 tag 到 node 编号的 map
  for(int i = 0; i < NN; i++)
    nTag2Nid[nodeTag[i]] = i;

  std::vector<int> nodedim(NN);
  std::vector<int> nodetag(NN);
  gmsh::vectorpair dim2d_tags;
  gmsh::model::getEntities(dim2d_tags, 2);
  for(const auto & it : dim2d_tags)
  {
    const int tag = it.second;
    std::vector<int> cellType; //单元的类型
    std::vector<std::vector<long unsigned int> > cellTag; //单元的标签
    std::vector<std::vector<long unsigned int> > c2nList; //单元的顶点 tag
    gmsh::model::mesh::getElements(cellType, cellTag, c2nList, 2, tag);// 2 维, tag 为 id

    std::vector<long unsigned int> cellTag1 = cellTag[0];
    std::vector<long unsigned int> c2nList1 = c2nList[0];      
    int NC = cell.size();
    int NC0 = cellTag1.size();
    cell.resize(NC+NC0);
    for(int i = 0; i < NC0; i++)
    {
      for(int j = 0; j < 3; j++)
      {
        cell[NC+i][j] = nTag2Nid[c2nList1[3*i+j]]; //单元
        nodetag[cell[NC+i][j]] = tag; //出现的点都属于几何中的 tag 
        nodedim[cell[NC+i][j]] = 2; //出现的点都是 2 维的
      }
    }
  }

  gmsh::vectorpair dim1d_tags;
  gmsh::model::getEntities(dim1d_tags, 1);
  for(const auto & it : dim1d_tags)
  {
    const int tag = it.second;
    std::vector<int> cellType; //单元的类型
    std::vector<std::vector<long unsigned int> > cellTag; //单元的标签
    std::vector<std::vector<long unsigned int> > c2nList; //单元的顶点 tag
    gmsh::model::mesh::getElements(cellType, cellTag, c2nList, 1, tag);// 2 维, tag 为 id

    std::vector<long unsigned int> cellTag1 = cellTag[0];
    std::vector<long unsigned int> c2nList1 = c2nList[0];      
    int N = c2nList1.size();
    for(int i = 0; i < N; i++)
    {
      int A = nTag2Nid[c2nList1[i]]; //单元
      nodetag[A] = tag; //出现的点都属于几何中的 id 
      nodedim[A] = 1; //出现的点都是 3 维的
    }
  }

  gmsh::vectorpair dim0d_tags;
  gmsh::model::getEntities(dim0d_tags, 0);
  for(const auto & it : dim0d_tags)
  {
    const int tag = it.second;
    std::vector<int> cellType; //单元的类型
    std::vector<std::vector<long unsigned int> > cellTag; //单元的标签
    std::vector<std::vector<long unsigned int> > c2nList; //单元的顶点 tag
    gmsh::model::mesh::getElements(cellType, cellTag, c2nList, 0, tag);// 2 维, tag 为 id

    std::vector<long unsigned int> cellTag1 = cellTag[0];
    std::vector<long unsigned int> c2nList1 = c2nList[0];      
    int N = c2nList1.size();
    for(int i = 0; i < N; i++)
    {
      int A = nTag2Nid[c2nList1[i]]; //单元
      nodetag[A] = tag; //出现的点都属于几何中的 id 
      nodedim[A] = 0; //出现的点都是 3 维的
    }
  }
  mesh->init_top();
  auto & data = mesh->get_node_int_data();
  data["gdof"] = nodedim;
  data["gtag"] = nodetag;
}

void construct_mesh3d(TetMesh * mesh)
{
  std::vector<long unsigned int> nodeTag; //网格中每个节点的 tag
  std::vector<double> nodeCoord; //每个节点的坐标
  std::vector<double> nodeParaCoord; //每个节点的参数坐标, 暂时不知道什么用
  gmsh::model::mesh::getNodes(nodeTag, nodeCoord, nodeParaCoord);

  int NN = nodeTag.size();
  auto & node = mesh->nodes();
  auto & cell = mesh->cells();
  node.resize(NN);
  for(int i = 0; i < NN; i++)
  {
    node[i][0] = nodeCoord[3*i+0];
    node[i][1] = nodeCoord[3*i+1];
    node[i][2] = nodeCoord[3*i+2];
  }

  std::map<int, int> nTag2Nid; //node 的 tag 到 node 编号的 map
  for(int i = 0; i < NN; i++)
    nTag2Nid[nodeTag[i]] = i;

  std::vector<int> nodedim(NN);
  std::vector<int> nodetag(NN);
  gmsh::vectorpair dim3d_tags;
  gmsh::model::getEntities(dim3d_tags, 3);
  for(const auto & it : dim3d_tags)
  {
    const int tag = it.second;
    std::vector<int> cellType; //单元的类型
    std::vector<std::vector<long unsigned int> > cellTag; //单元的标签
    std::vector<std::vector<long unsigned int> > c2nList; //单元的顶点 tag
    gmsh::model::mesh::getElements(cellType, cellTag, c2nList, 3, tag);// 2 维, tag 为 id

    std::vector<long unsigned int> cellTag1 = cellTag[0];
    std::vector<long unsigned int> c2nList1 = c2nList[0];      
    int NC = cell.size();
    int NC0 = cellTag1.size();
    cell.resize(NC+NC0);
    for(int i = 0; i < NC0; i++)
    {
      for(int j = 0; j < 4; j++)
      {
        cell[NC+i][j] = nTag2Nid[c2nList1[4*i+j]]; //单元
        nodetag[cell[NC+i][j]] = tag; //出现的点都属于几何中的 tag 
        nodedim[cell[NC+i][j]] = 3; //出现的点都是 2 维的
      }
    }
  }

  gmsh::vectorpair dim2d_tags;
  gmsh::model::getEntities(dim2d_tags, 2);
  for(const auto & it : dim2d_tags)
  {
    const int tag = it.second;
    std::vector<int> cellType; //单元的类型
    std::vector<std::vector<long unsigned int> > cellTag; //单元的标签
    std::vector<std::vector<long unsigned int> > c2nList; //单元的顶点 tag
    gmsh::model::mesh::getElements(cellType, cellTag, c2nList, 2, tag);// 2 维, tag 为 id

    std::vector<long unsigned int> cellTag1 = cellTag[0];
    std::vector<long unsigned int> c2nList1 = c2nList[0];      
    int N = c2nList1.size();
    for(int i = 0; i < N; i++)
    {
      int A = nTag2Nid[c2nList1[i]]; //单元
      nodetag[A] = tag; //出现的点都属于几何中的 tag 
      nodedim[A] = 2; //出现的点都是 2 维的
    }
  }

  gmsh::vectorpair dim1d_tags;
  gmsh::model::getEntities(dim1d_tags, 1);
  for(const auto & it : dim1d_tags)
  {
    const int tag = it.second;
    std::vector<int> cellType; //单元的类型
    std::vector<std::vector<long unsigned int> > cellTag; //单元的标签
    std::vector<std::vector<long unsigned int> > c2nList; //单元的顶点 tag
    gmsh::model::mesh::getElements(cellType, cellTag, c2nList, 1, tag);// 2 维, tag 为 id

    std::vector<long unsigned int> cellTag1 = cellTag[0];
    std::vector<long unsigned int> c2nList1 = c2nList[0];      
    int N = c2nList1.size();
    for(int i = 0; i < N; i++)
    {
      int A = nTag2Nid[c2nList1[i]]; //单元
      nodetag[A] = tag; //出现的点都属于几何中的 id 
      nodedim[A] = 1; //出现的点都是 3 维的
    }
  }

  gmsh::vectorpair dim0d_tags;
  gmsh::model::getEntities(dim0d_tags, 0);
  for(const auto & it : dim0d_tags)
  {
    const int tag = it.second;
    std::vector<int> cellType; //单元的类型
    std::vector<std::vector<long unsigned int> > cellTag; //单元的标签
    std::vector<std::vector<long unsigned int> > c2nList; //单元的顶点 tag
    gmsh::model::mesh::getElements(cellType, cellTag, c2nList, 0, tag);// 2 维, tag 为 id

    std::vector<long unsigned int> cellTag1 = cellTag[0];
    std::vector<long unsigned int> c2nList1 = c2nList[0];      
    int N = c2nList1.size();
    for(int i = 0; i < N; i++)
    {
      int A = nTag2Nid[c2nList1[i]]; //单元
      nodetag[A] = tag; //出现的点都属于几何中的 id 
      nodedim[A] = 0; //出现的点都是 3 维的
    }
  }
  mesh->init_top();
  auto & data = mesh->get_node_int_data();
  data["gdof"] = nodedim;
  data["gtag"] = nodetag;

  int NC = mesh->number_of_cells();
  std::cout << "单元个数 ：" << NC << std::endl;
  for(int i = 0; i < NC; i++)
  {
    double v = mesh->cell_measure(i);
    if(v <= 0.0)
      std::cout << "翻转单元" << std::endl;
  }
}

int main(int argc, char ** argv)
{
  argc++;
  gmsh::initialize();
  gmsh::open(argv[1]);
  gmsh::model::geo::synchronize();
  gmsh::model::mesh::generate(3);
  //gmsh::model::mesh::refine();
  //gmsh::model::mesh::refine();

  TetMesh mesh;
  construct_mesh3d(&mesh);
  
  Writer writer;
  writer.set_points(mesh);
  writer.set_cells(mesh);
  writer.set_point_data(mesh.get_node_int_data()["gtag"], 1, "gtag");
  writer.set_point_data(mesh.get_node_int_data()["gdof"], 1, "gdof");
  writer.write("init_tet_mesh.vtu");

  gmsh::finalize();
  return 0;
}




