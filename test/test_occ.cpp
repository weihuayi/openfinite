#include <QCoreApplication>
#include <BRepTools.hxx>
#include <BRep_Builder.hxx>
#include <unistd.h>

int main(int argc, char *argv[])
{
    BRep_Builder brbp;
    QCoreApplication a(argc, argv);

    TopoDS_Shape entityOCC;
    bool ret = BRepTools::Read(entityOCC,Standard_CString("draw.brep"), brbp);
    if(ret == false)
    {
        printf("BREP read failed\n");fflush(0);
    }else{
        printf("CAD brep loaded\n");fflush(0);
    }
    if(entityOCC.IsNull())
    {
        printf("Null Shape\n");fflush(0);
    }
    return a.exec();
}
