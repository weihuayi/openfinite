#include <gmsh.h>
#include <vector>
#include <iostream>

void test_project_point()
{
  gmsh::initialize();
  //add a circle
  int c = gmsh::model::occ::addCircle(0, 0, 0, 1);
  gmsh::model::occ::synchronize();

  //find closet point to (1.3, 1.3) by orthogonal projection on the curve c
  std::vector<double> point({0.8, 0.8, 0});
  std::vector<double> close_p, close_pp;
  gmsh::model::getClosestPoint(1, c, point, close_p, close_pp);

  //add a point on the projection
  gmsh::model::occ::addPoint(point[0], point[1], point[2]);
  gmsh::model::occ::addPoint(close_p[0], close_p[1], close_p[2]);

  gmsh::model::occ::synchronize();
  gmsh::fltk::run();
  gmsh::finalize();
}

void test_get_tangent()
{
  gmsh::initialize();
  //add a circle
  int c = gmsh::model::occ::addCircle(0, 0, 0, 1);
  gmsh::model::occ::synchronize();

  //find closet point to (1.3, 1.3) by orthogonal projection on the curve c
  std::vector<double> point({std::sqrt(2)/2.0, std::sqrt(2)/2.0, 0});
  std::vector<double> point_p, der;
  gmsh::model::getParametrization(1, c, point, point_p);

  // 获取切向
  gmsh::model::getDerivative(1, c, point_p, der);

  std::cout << der.size() << " " << der[0] << " " << der[1] << " " << der[2] << std::endl;

  //add a point on the projection
  gmsh::model::occ::synchronize();
  gmsh::finalize();
}

void test_get_normal()
{
  gmsh::initialize();
  //add a circle
  int c = gmsh::model::occ::addSphere(0, 0, 0, 1);
  gmsh::model::occ::synchronize();

  std::vector<double> point({std::sqrt(2)/2.0, std::sqrt(2)/2.0, 0});
  std::vector<double> point_p, norm;
  gmsh::model::getParametrization(2, c, point, point_p);

  // 获取法向
  gmsh::model::getNormal(c, point_p, norm);

  std::cout << norm.size() << " " << norm[0] << " " << norm[1] << " " << norm[2] << std::endl;

  //add a point on the projection
  gmsh::model::occ::synchronize();
  gmsh::finalize();
}

int main()
{
  //test_project_point();
  test_get_tangent();
  test_get_normal();
  return 0;
}


