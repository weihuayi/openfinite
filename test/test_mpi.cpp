#include "mpi.h"
#include <gmsh.h>
#include <iostream>
#include <string>
#include <sstream>

int main(int argc, char * argv[])
{
  MPI_Init(&argc, &argv);

  int rank, nprocs;
  //MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  std::cout << rank << std::endl;

  /*
  std::stringstream ss;
  ss << argv[1] << "_" << rank << ".geo";
  //model_file_name << argv[1] << ".geo";
  gmsh::initialize();
  gmsh::open(ss.str());

  gmsh::open(argv[1]);

  gmsh::finalize();
  */
  MPI_Finalize();
  return 0;
}

